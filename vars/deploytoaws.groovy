#!/user/bin/env groovy

def call(String ec2Instance, String dockerImage) {
    def fileName = 'docker-compose.yaml'
    def shellCmd = "docker-compose -f ${fileName} up --detach"
    def dockerImgCmd = "export DOCKER_IMAGE=${dockerImage}"
    def combinedCmd = "${dockerImgCmd} && ${shellCmd}"

    //Remember to set or update this key because it is linked to a multi pipeline branch
    //For multibranch -test1 credentials set using ssh-agent plugin ec2-server-key
    //for ec2-multi-branch-pipeline is devops-secret-key

    sshagent(['ec2-devops-server-key']) {
        //sh 'ssh -o StrictHostKeyChecking=no ec2-user@54.170.74.12 "echo Successfully connected"'
        sh "scp -o StrictHostKeyChecking=no ${fileName} ${ec2Instance}:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} '${combinedCmd}'"


    }

}
