#!/user/bin/env groovy

def call() {


        script {

            echo 'Incrementing node version'
            sh 'node --version'
            echo 'Pre increment version'
            echo ''
            sh 'npm version minor'
            def packageJson = readJSON file: 'package.json'
            def version = packageJson.version
            // Extract the updated version from package.json
            def newVersion = "$version-$BUILD_NUMBER"

            echo "Post-increment version: ${newVersion}"

            // Return the new version
            return "$newVersion"
        }


}