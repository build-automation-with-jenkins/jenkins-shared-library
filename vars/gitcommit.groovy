#!/user/bin/env groovy

def call() {
    echo 'Committing version to git'
    withCredentials([usernamePassword(credentialsId: 'gitlab', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'git config user.email "jenkins@example.com"'
        sh 'git config user.name "jenkins"'


        sh 'git status'
        sh 'git branch'
        sh 'git config --list'


        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/aws-services2/jenkins-ec2-deployment.git"

        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push origin HEAD:master'

    }
}