#!/user/bin/env groovy

def call(String imageName) {
    echo "Building docker image with this package version ${imageName}"

        withCredentials([usernamePassword(credentialsId: 'DockerHub', passwordVariable: 'PASS', usernameVariable: 'USER')]){
            sh "docker build -t $imageName ."
            sh 'echo $PASS | docker login -u $USER --password-stdin'
            sh "docker push $imageName"

        }


}