#### Jenkins Shared Library
</details>

***
This is a node js Jenkins Pipeline shared library. Repositories with similar Pipeline setup, that have stages listed below can all use this share library:

1. Build Docker Image.
2. Git commit. To commit changes made by jenkins back to gitlab
3. Increment or version bump.
4. Push docker image to private repository like DockerHub.
5. Run test.

All these stages have their respective files within the var folder.
These files are referenced as a function. They are groovy scripts.

Rather than have these stages replicated inside every Jenkinsfile contained inside the respective repositories, it is best to use a shared library such as this.

<details>


**<summary>Basic Setup Steps</summary>**
 <br />
Within manage Jenkins, this was setup as a Global Pipeline Libraries under Global System configuration

- Provide library Name.
- Default version was main so that latest version will always be used.
- Every function is the name of the groovy script and will be referenced in the Jenkinsfile that will be used by the Pipeline.
- Referenced in the Jenkinsfile as @Library('Library_Name') at the top of the file, just before pipeline attibute

```sh


```

</details>

******



